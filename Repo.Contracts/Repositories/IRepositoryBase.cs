﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repo.Contracts.Repositories
{
    public interface IRepositoryBase<TEntity>
        where TEntity : class
    {
        TEntity getSingle(object id);
        IQueryable<TEntity> getALL();
        void Insert(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);
        void Commit();
    }
}
