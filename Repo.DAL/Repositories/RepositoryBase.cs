﻿using Repo.Contracts.Repositories;
using Repo.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repo.DAL.Repositories
{
    
    public abstract class RepositoryBase<TEntity> : IRepositoryBase<TEntity> where TEntity:class
    {
        internal DBContext context;
        internal DbSet<TEntity> dbSet;

        public RepositoryBase(DBContext context)
        {
            this.context = context;
            this.dbSet = context.Set<TEntity>();
        }

        public virtual TEntity getSingle(object id)
        {
            return dbSet.Find(id);
        }

        public virtual IQueryable<TEntity> getALL()
        {
            return dbSet;
        }

        public virtual void Insert(TEntity entity)
        {
            dbSet.Add(entity);
        }
        public virtual void Update(TEntity entity)
        {
            dbSet.Attach(entity);
            context.Entry(entity).State = EntityState.Modified;
        }
        public virtual void Delete(TEntity entity)
        {
            if (context.Entry(entity).State == EntityState.Detached)
                dbSet.Attach(entity);
            dbSet.Remove(entity);
        }
        public virtual void Commit()
        {
            context.SaveChanges();
        }
    }
}
