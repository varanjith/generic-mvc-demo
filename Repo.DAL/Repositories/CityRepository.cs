﻿using Repo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repo.DAL.Repositories
{
    public class CityRepository:RepositoryBase<City>
    {
        public CityRepository(DBContext context) : base(context)
        {
            if (context == null)
                throw new ArgumentNullException();
        }
    }
}
