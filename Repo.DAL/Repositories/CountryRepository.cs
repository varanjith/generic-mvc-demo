﻿using Repo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repo.DAL.Repositories
{
    public class CountryRepository:RepositoryBase<Country>
    {
        public CountryRepository(DBContext context):base(context)
        {
            if (this.context == null)
                throw new ArgumentNullException();
        }
    }
}
