﻿using Repo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repo.DAL.Repositories
{
    public class StateRepository : RepositoryBase<State>
    {
        public StateRepository(DBContext context) : base(context)
        {
            if (this.context == null)
                throw new ArgumentNullException();
        }
 
    }
}
