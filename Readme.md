This project shows the perfect use of Repository pattern and IoC.
I coded this one to show that my projects are standard, uses less 
code and looks neat. I handled inheritance in a way, that basic functions 
like CRUD are coded in a base class and inherited in all the repositories 
(i.e CountryRepository and StateRepository in this project). In other words, 
I created a generic function "Insert" and pass a State model which saves the
state. If we pass country model, the country is saved. Same function used everywhere.
That's the power of inheritance.