﻿using Repo.Contracts.Repositories;
using Repo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Repo.WebUI.Controllers
{
    public class CityController : Controller
    {
        internal IRepositoryBase<State> state;
        internal IRepositoryBase<Country> country;
        internal IRepositoryBase<City> city;
        public CityController(IRepositoryBase<State> state, IRepositoryBase<Country> country, IRepositoryBase<City> city)
        {
            this.state = state;
            this.country = country;
            this.city = city;
        }
        // GET: City
        public ActionResult Index()
        {
            return View(city.getALL());
        }
        public ActionResult Create()
        {
            ViewBag.StateList = new SelectList(state.getALL(), "Id", "Name");
            return View();
        }
        [HttpPost]
        public ActionResult Create(City city)
        {
            this.city.Insert(city);
            this.city.Commit();
            return RedirectToAction("Index");
        }
    }
}