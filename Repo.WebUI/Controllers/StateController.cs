﻿using Repo.Contracts.Repositories;
using Repo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Repo.WebUI.Controllers
{
    public class StateController : Controller
    {
        internal IRepositoryBase<State> state;
        internal IRepositoryBase<Country> country;

        public StateController(IRepositoryBase<State> state,IRepositoryBase<Country> country)
        {
            this.state = state;
            this.country = country;
        }
        // GET: state
        public ActionResult Index()
        {
            return View(state.getALL());
        }

        public ActionResult Create()
        {
            ViewBag.CountryList = new SelectList(country.getALL(),"Id","Name");
            return View();
        }
        [HttpPost]
        public ActionResult Create(State state)
        {
            this.state.Insert(state);
            this.state.Commit();

            return RedirectToAction("Index");
        }

        public ActionResult Edit(int? id)
        {
            ViewBag.CountryList = new SelectList(country.getALL(), "Id", "Name");
            return View(state.getSingle(id));
        }
        [HttpPost]
        public ActionResult Edit(State state)
        {
            this.state.Update(state);
            this.state.Commit();
            return RedirectToAction("Index");
        }

        public ActionResult Details(int? id)
        {
            return View(state.getSingle(id));
        }

        public ActionResult Delete(int? id)
        {
            return View(state.getSingle(id));
        }
        [HttpPost]
        public ActionResult Delete(State state)
        {
            this.state.Delete(state);
            this.state.Commit();
            return RedirectToAction("Index");
        }
    }
}