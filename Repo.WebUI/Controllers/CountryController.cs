﻿using Repo.Contracts.Repositories;
using Repo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Repo.WebUI.Controllers
{
    public class CountryController : Controller
    {
        internal IRepositoryBase<Country> country;

        public CountryController(IRepositoryBase<Country> country)
        {
            this.country = country;
        }
        // GET: Country
        public ActionResult Index()
        {
            return View(country.getALL());
        }

        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Country country)
        {
            this.country.Insert(country);
            this.country.Commit();
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int? id)
        {
            return View(country.getSingle(id));
        }
        [HttpPost]
        public ActionResult Edit(Country country)
        {
            this.country.Update(country);
            this.country.Commit();
            return RedirectToAction("Index");
        }

        public ActionResult Details(int? id)
        {
            return View(country.getSingle(id));
        }

        public ActionResult Delete(int? id)
        {
            return View(country.getSingle(id));
        }
        [HttpPost]
        public ActionResult Delete(Country country)
        {
            this.country.Delete(country);
            this.country.Commit();
            return RedirectToAction("Index");
        }
    }
}